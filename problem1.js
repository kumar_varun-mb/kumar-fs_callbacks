/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/

const fs = require('fs');
const path = require('path');

function createJsonFile(dir, data, callback){
    fs.writeFile(dir, data, 'utf-8', (err) => {
        if(err)
            console.log(err);
        else{
            console.log('JSON File created.');
            callback(dir);
        }
    });
}

function deleteJsonFile(dir){
    fs.unlink(dir, (err) => {
        if(err)
            console.log(err);
        else
            console.log('JSON File deleted.');
    });
}

module.exports = {createJsonFile, deleteJsonFile};