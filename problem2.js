/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require('fs');
const path = require('path');

function readFile(){
    let file;
    // 1. Reading file 'lipsum.txt'.
    fs.readFile(                
        path.join(__dirname, './data/lipsum.txt'), 'utf-8',
        (err, data) => {
            if(err)
                console.log(err);
            else{          //2.1 If File read successful then convert it to UpperCase and write to another File.
                fs.writeFile(   
                    path.join(__dirname, './data/UpperCaseLipsum.txt'), data.toUpperCase(), 'utf-8', 
                    (err) => {
                        if(err)
                            console.log(err);
                        else{
                            console.log('UpperCaseLipsum.txt created. Check directory!');
                            fs.appendFile(
                                path.join(__dirname, './data/filenames.txt'), 'UpperCaseLipsum.txt\n',
                                (err) => {
                                    if (err) console.log(err);
                                    else console.log('filenames.txt appended');
                                  }
                            );  // 2.2 New File Name appended to filenames.txt
                            
                            // 3.1 Now reading UpperCaseLipsum.txt and converting to LowerCase
                            fs.readFile(path.join(__dirname, './data/UpperCaseLipsum.txt'),'utf-8',
                                  (err,data) => {
                                      if(err)   console.log(err);
                                      else {
                                        const lowerCaseData = data.toLowerCase();
                                        fs.writeFile(path.join(__dirname, './data/splitLipsum.txt'), 
                                        lowerCaseData.split('.').map((str)=> {
                                            return str.trim();
                                        }).join('\n'),  //3.2 Converted the file data to lowerCase and split into sentences, stored in a new file.
                                        'utf-8',
                                        (err) => {
                                            if(err)
                                                console.log(err);
                                            else{
                                                console.log('Text data has been split into sentences and stored in `splitLipsum.txt` file.');
                                                fs.appendFile(      //3.3 Storing name of new file in filenames.txt
                                                    path.join(__dirname, './data/filenames.txt'), 'splitLipsum.txt\n',
                                                    (err) => {
                                                        if (err) console.log(err);
                                                        else console.log('Data converted to lowercase, split into sentences and stored in `splitLipsum.txt`. File name appended to filenames.txt');
                                                      });
                                                // 4.1 Read the new file, sort contents.
                                                fs.readFile(path.join(__dirname, './data/splitLipsum.txt'),'utf-8',
                                                      (err,data)=>{
                                                        if(err) console.log(err);
                                                        else{
                                                            // 4.2 Store sorted data to new file.                                         
                                                            fs.writeFile(path.join(__dirname, './data/sortSplitLipsum.txt'), 
                                                            data.split('\n').sort().join('\n'), 'utf-8',
                                                            (err) => {
                                                                if(err) console.log(err);
                                                                else{
                                                                    fs.appendFile(      //3.3 Storing name of new file in filenames.txt
                                                                        path.join(__dirname, './data/filenames.txt'), 'sortSplitLipsum.txt',
                                                                        (err) => {
                                                                        if (err) console.log(err);
                                                                        else{
                                                                            console.log('Contents of splitLipsum.txt sorted and stored in a new file `sortSplitLipsum.txt`. New file name appended to filenames.txt');
                                                                            fs.readFile(path.join(__dirname, './data/filenames.txt'), 'utf-8',
                                                                            (err, data) => {
                                                                                if(err) console.log(err);
                                                                                else{
                                                                                    fileNames = data.split('\n');
                                                                                    fileNames.forEach((file) => {
                                                                                        fs.unlink(path.join(__dirname, `./data/${file}`), (err) => {
                                                                                            if(err) console.log('Delete error');
                                                                                            else
                                                                                                console.log(`Deleted ${file}`);
                                                                                        }

                                                                                        )
                                                                                    })
                                                                                }
                                                                            }
                                                                            );
                                                                        }
                                                                    });
                                                                }
                                                            }

                                                            );
                                                        }
                                        
                                                      }
                                                );

                                            }
                                        }
                                        );
                                      }
                                  }
                            )
                            
                        }
                    }
                );
            }
        });
    }

readFile();
